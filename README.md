![gif of tsunami waves at two different friction values.](tsunami_friction.gif)

Development of tsunami waves over time, moving along a seabed with two different friction values.

# Fork
## Reason for fork
The purpose of this fork is to showcase the effect of friction on tsunami waves, primarily through the adaptation of a pre-existing notebook in the original clawpack/apps repository, adapted to: [chile2010b_alt.ipynb](chile2010b/chile2010b_alt.ipynb). It was created as part of an undergraduate project.

## Changes made in this fork
- There are changes in both the Chile2010a and Chile2010b directories. The main new notebook (to be ran) is [chile2010b_alt.ipynb](chile2010b/chile2010b_alt.ipynb). 
- There is the addition of a [CLAWdoc_install_issues.md](CLAWdoc_install_issues.md) file to aid with installation.
-The addition of [gauge32412-actual-detide.png](chile2010b/gauge32412-actual-detide.png) file - modified from the dart32412_comp-2.png file in /clawpack_src/clawpack-$CLAW_VERSION/geoclaw/examples/tsunami/chile2010. This image is used to plot the simulated wave height against the real-life data.

# Notes
- This is my first repository on gitlab, and I am really using this as a quick means to an end to show people who need to view my work. Thus, I do not intend to become familiar with and adhere to good practices of maintaining a repository. Therefore please do not take this to be anything other than a fork for my own personal use, but that if you wish to use, you are more than welcome to. 

- Since this is a work in progress, the markdown blocks in the chile2010b_alt.ipynb may have less detail than desired. They contain descriptions I feel are pertinent to the program, but if you are looking for more detail - I recommend first going through the chile2010a.ipynb and chile2010b.ipynb files, as this is the original CLAWPACK version, and then move to mine to observe the changes I've made.

# Installation

The following installation was developed on a Linux (manjaro) machine, however the code here should be applicable to all unix-based operating systems.

## External dependencies

If the following are not already installed:

- Install [pip](https://pip.pypa.io/en/stable/installing) (if not already installed).

- Install [gfortran](https://gcc.gnu.org/wiki/GFortranBinaries) (You can use a different fortran compiler, but change the parts which mention 'gfortran' to the name of your fortran compiler).

- [Install](https://jupyter.org/install) Jupyterlab or classic Jupyter notebook.

If `vim` is not your preferred text editor, insert the name of your chosen editor instead (e.g `vi`, `nano`, etc.).

## CLAWPACK dependencies

[CLAWPACK](https://www.clawpack.org/v5.7.x/index.html) version 5.7.0, the [CLAWPACK Applications repository](https://www.clawpack.org/apps.html) version 5.6.0, and [pip module CLAWPACK](https://pypi.org/project/clawpack/5.7.0/) version 5.7.0; are required for this software. If you do not have these installed already, the follow shows my preferred installation of these.

1. Write environment variables: FC, CLAW, CLAW_VERSION; to .profile configuration file.

`sudo vim ~/.profile`

Enter the following lines at the bottom of the .profile file:
```
export CLAW_VERSION=v5.7.0 #CLAWPACK version for bittern/BotFricTsunami
export CLAW=$HOME/clawpack_src/clawpack-$CLAW_VERSION #CLAWPACK directory
export FC=gfortran #Name of Fortran compiler
```
Save and exit the editor.

2. Now reboot your machine to allow python to read new environment variables.

3. Install CLAWPACK pre-requisites
My preferred method of installing CLAWPACK is via git, inspired by the [documentation](https://www.clawpack.org/v5.7.x/installing_more_options.html#git-clone)
```
git clone git://github.com/clawpack/clawpack.git $CLAW --branch $CLAW_VERSION
cd $CLAW
git submodule init      # for repositories pyclaw, clawutil, visclaw, etc.
git submodule update    # clones all the submodule repositories
git clone git://github.com/clawpack/apps.git apps --branch v5.6.0
pip --no-cache-dir install clawpack==5.7.0 # flag --no-cache-dir avoids conflicting versions if you have another clawpack version installed via pip
```

## Installing this repository

I recommend the repository is installed in the $CLAW/apps/notebooks/ directory as in the following line of code:
```
cd $CLAW
git clone https://gitlab.com/bittern/BotFricTsunami.git apps/notebooks/BotFricTsunami
```

# How to use
Start either JupyterLab or classic Jupyter notebook via the respective commands:

```
jupyter lab
```
or
```
jupyter notebook
```

And enter the outputted URL in your browser. Once inside the jupyter software, navigate to chile2010b_alt.ipynb, and run the notebook. If you followed the above Installation section, please contact me if you've had issues. If you encounter problems, and have followed the CLAWPACK installation guide as given [here](https://www.clawpack.org/installing.html#pip-install); first ensure you have installed the version of CLAWPACK, CLAWPACK/apps, and the pip version of CLAWPACK this notebook requires, then either check out the CLAWdoc_install_issues.md file - which documents solutions to the issues I encountered during installation - or start again, following the Installation section above. Please note that in the Makefile of the Chile2010a and Chile2010b directories, I have inserted the flag detailed in the CLAWdoc_install_issues.md file, shown in the problem titled 'Type mismatch - fortran'. If you encounter an error with a Makefile, please take off the flag and re-run. If this does not solve your issue, I suggest you search online.

# More information
For more information on the topic, check out the [poster](Poster-Bottom_Friction-Tsunami.pdf) made for this project, and this [file](More_info-Bottom_Friction-Tsunami.pdf) for much more information about the mathematics - and choices made - behind this model.


# Attribution

Repository icon made by *dDara* from [Flaticon](www.flaticon.com)
